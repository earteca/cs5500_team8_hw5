package com.example.teamgr8.services;

import com.example.teamgr8.utils.PythonParserImpl;
import org.antlr.v4.runtime.ParserRuleContext;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
@RestController
public class ParserService {
    PythonParserImpl antlrOps = new PythonParserImpl();

    @GetMapping("api/greet/{str}")
    public String test(@RequestBody String str){
        return "Hello "+str;
    }

    @PostMapping("api/compare")
    public String test2(@RequestBody String str){
        return "Hello "+str;
    }

    @PostMapping("api/parse")
    public void parseTree(@RequestBody String filePath) throws IOException {
        ParserRuleContext parseTree = antlrOps.getParserRuleContext(filePath);
        System.out.println(parseTree.getChild(0).getText());
        System.out.println(parseTree.getChild(1).getText());
    }
}
