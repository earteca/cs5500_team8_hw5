package com.example.teamgr8.utils;

import com.example.teamgr8.models.Result;
import com.example.teamgr8.models.Submission;

import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;

import java.io.IOException;
import java.util.*;

public class PlagiarismDetectorImpl implements PlagiarismDetector {

    @Override
    public Result checkPlagiarism(Submission sub1, Submission sub2, ASTParser parser) throws IOException {
	return new Result();
    }

    private Map<Integer, List<Integer>> createTokenMap(CommonTokenStream tokenStream) {

        Map<Integer, List<Integer>> tokenMap = new HashMap<>();

        return tokenMap;
    }

    private boolean validTokenCheck(Token token) {
	return false;
    }

    private List<Integer> findMatchingLines(Map<Integer, List<Integer>> subBeingCheckedLines,
                                            Map<Integer, List<Integer>> subToCompareToLines) {

        List<Integer> matchingLines = new ArrayList<>();
        return matchingLines;
    }

    private List<String> getLineContent(Submission sub, List<Integer> matchingLinesInSub) throws IOException {
        List<String> linesToReturn = new ArrayList<>();

        return linesToReturn;
    }

    private double percentageCalc(int matchCount, long lineCount) {
	return 1;
    }

}
