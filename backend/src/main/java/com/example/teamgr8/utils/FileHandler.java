package com.example.teamgr8.utils;

import com.example.teamgr8.models.Languages;
import com.example.teamgr8.models.Result;
import com.example.teamgr8.models.Submission;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface FileHandler {
    /**
     * This takes in an homework path and iterates through the student subfolders and generates the
     * submission map for the particular homework.
     *
     * @param hwFilePath filepath of the homework.
     * @return SubmissionMap for the particular homework.
     */
    Map<Languages, List<Submission>> loadHomework(String hwFilePath) throws IOException;

    /**
     * Takes in a list of results for a particular homework and writes it to the results folder.
     *
     * @param hwFilePath homework filepath.
     * @param result result object
     * @throws IOException throws IOException in case folder doesnt exist.
     */
    void writeResults(String hwFilePath, List<Result> result) throws IOException;

    /**
     * Takes in a homework filepath and returns a list of results.
     *
     * @param hwFilePath homework filepath.
     * @param results returns a list of results loaded from the results folder.
     * @throws IOException throws IOException in case folder doesnt exist.
     */
    List<Result> loadResult(String hwFilePath) throws FileNotFoundException;
}
