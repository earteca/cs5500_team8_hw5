package com.example.teamgr8.utils;

import com.example.teamgr8.models.Languages;
import com.example.teamgr8.models.Result;
import com.example.teamgr8.models.Submission;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.ArrayList;

public class FileHandlerImpl implements FileHandler {

  Map<Languages, List<Submission>> hwMap;
  Gson gson;

  public FileHandlerImpl() {
    gson = new Gson();
    hwMap = new HashMap<>();
    hwMap.put(Languages.PYTHON, new ArrayList<>());
    hwMap.put(Languages.HTML, new ArrayList<>());
  }

  public static void main(String[] args) throws IOException {
    FileHandlerImpl abc = new FileHandlerImpl();
    Map<Languages, List<Submission>> hwMap = abc.loadHomework("files/HW1");
    System.out.println("sadasd");

    List<Result> results = new ArrayList<>();
    Result result1 = new Result("Student_1", "Student2", "files/HW1/Student_1/tinycode.py"
        , "files/HW1/Student_2/tinycode.py", null, null, 4, 50,
        50, null, null);
    Result result2 = new Result("Student_1", "Student2", "files/HW1/Student_1/src/tinycode.py"
        , "files/HW1/Student_2/tinycode.py", null, null, 4, 40,
        40, null, null);
    Result result3 = new Result("Student_1", "Student2", "files/HW1/Student_1/src/tinycode.py"
        , "files/HW1/Student_2/tinycode.py", null, null, 4, 30,
        90, null, null);
    results.add(result1);
    results.add(result2);
    results.add(result3);

    Collections.sort(results,Collections.reverseOrder());


    List<Result> loadedResults = abc.loadResult("files/HW1");

    abc.writeResults("files/HW1", results);
  }


  @Override
  public Map<Languages, List<Submission>> loadHomework(String hwFilePath) throws IOException {
    File[] studentDirectories = new File(hwFilePath).listFiles(File::isDirectory);
    if (studentDirectories != null) {
      for (File studentDirectory : studentDirectories) {
        processStudentFolder(studentDirectory);
      }
    }
    return this.hwMap;
  }

  @Override
  public void writeResults(String hwFilePath, List<Result> results) throws IOException {
    new File(hwFilePath + "/results").mkdir();
    int counter = 0;
    for (Result result : results) {
      String resultFilename = result.getStudent_id1() + result.getStudent_id2() + counter;
//      gson.toJson(result, new FileWriter(hwFilePath + "/results/" + resultFilename));
      try (FileWriter writer = new FileWriter(
          hwFilePath + "/results/" + resultFilename + ".json")) {
        gson.toJson(result, writer);
      } catch (IOException e) {
        e.printStackTrace();
      }
      String jsonInString = gson.toJson(result);
      counter++;
    }
  }

  @Override
  public List<Result> loadResult(String hwFilePath) throws FileNotFoundException {
    File resultsFolder = new File(hwFilePath + "/results");
    File[] files = resultsFolder.listFiles();
    List<Result> results = new ArrayList<>();
    if (files != null) {
      for (File file : files) {
        Result result = gson.fromJson(new FileReader(file), Result.class);
        results.add(result);
      }
    }
    return results;
  }

  private void processStudentFolder(File studentHwFilePath) throws IOException {
    File hwFolder = studentHwFilePath;
    String studentId = hwFolder.getName();

    // contains a list of  nested folder directories that have to be traversed
    List<File> nestedFolders = new ArrayList<>();
    nestedFolders.add(studentHwFilePath);

    while (!nestedFolders.isEmpty()) {
      File folder = nestedFolders.remove(0);
      File[] files = folder.listFiles();
      for (int i = 0; i < files.length; i++) {
        if (files[i].isDirectory()) {
          nestedFolders.add(files[i]);
        } else {
          String fileName = files[i].getName();
          Submission submission = new Submission(studentId, files[i].getPath());
          if (fileName.contains(".py")) {
            hwMap.get(Languages.PYTHON).add(submission);
          } else if (fileName.contains(".html")) {
            hwMap.get(Languages.HTML).add(submission);
          }
        }
      }
    }
  }
}
