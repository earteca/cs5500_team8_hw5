package com.example.teamgr8.utils;

import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;

public interface ASTParser {
    public ParserRuleContext getParserRuleContext(String code);

    //TODO is this necessary?
    public CommonTokenStream getTokens(String code);
}
