package com.example.teamgr8.utils;

import com.example.teamgr8.models.Result;
import com.example.teamgr8.models.Submission;

import java.io.IOException;

public interface PlagiarismDetector {
    Result checkPlagiarism(Submission sub1, Submission sub2, ASTParser parser) throws IOException;
}
