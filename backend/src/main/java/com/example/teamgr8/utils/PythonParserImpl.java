package com.example.teamgr8.utils;

import com.example.teamgr8.antlr4.python.Python3Lexer;
import com.example.teamgr8.antlr4.python.Python3Parser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class PythonParserImpl implements ASTParser {

  /**
   * If you want ParserRuleContext from string.
   *
   * @param code
   * @return
   */
  @Override
  public ParserRuleContext getParserRuleContext(String code) {
    Python3Lexer lexer = new Python3Lexer(new ANTLRInputStream(code));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    Python3Parser parser = new Python3Parser(tokens);
    return parser.file_input();
  }

  /**
   * If you want ParseTree from String.
   *
   * @param code
   * @return
   */
  public ParseTree getParseTree(String code) {
    ParseTree parseTree = this.getParserRuleContext(code).getChild(0);
    return parseTree;
  }


  @Override
  public CommonTokenStream getTokens(String code) {
    Python3Lexer lexer = new Python3Lexer(new ANTLRInputStream(code));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    tokens.fill();
    return tokens;
  }

}
