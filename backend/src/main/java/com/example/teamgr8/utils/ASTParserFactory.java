package com.example.teamgr8.utils;

public class ASTParserFactory {
    public ASTParser createPythonParser() {
        return new PythonParserImpl();
    }

    public ASTParser createHTMLParser(){
        return new HTMLParserImpl();
    }
}
