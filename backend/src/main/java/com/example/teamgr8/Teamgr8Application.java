package com.example.teamgr8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Teamgr8Application {

  public static void main(String[] args) {
    SpringApplication.run(Teamgr8Application.class, args);
  }


}
