package com.example.teamgr8.models;

import com.example.teamgr8.utils.FileHandler;
import com.example.teamgr8.utils.FileHandlerImpl;
import com.example.teamgr8.utils.ASTParser;
import com.example.teamgr8.utils.ASTParserFactory;
import com.example.teamgr8.utils.PlagiarismDetector;
import com.example.teamgr8.utils.PlagiarismDetectorImpl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeworkImpl implements Homework {

  private String hw_name;
  private String hw_filePath;
  private Map<Languages, List<Submission>> submissionMap;
  private PlagiarismDetector plagiarismDetectorPython;
  //TODO results should implement comparable and be sorted.
  private List<Result> results;
  private FileHandler fileHandler;
  private ASTParserFactory factory;
  private ASTParser parser;

  public HomeworkImpl(String hw_name, String hw_filePath,
      Map<Languages, List<Submission>> submissionMap) {
    this.hw_name = hw_name;
    this.hw_filePath = hw_filePath;
    this.submissionMap = submissionMap;
    this.plagiarismDetectorPython = new PlagiarismDetectorImpl();
    this.results = new ArrayList<>();
    this.factory = new ASTParserFactory();
    this.fileHandler = new FileHandlerImpl();
  }

  public Map<Languages, List<Submission>> getSubmissionMap() {
    return submissionMap;
  }

  public void setSubmissionMap(
      Map<Languages, List<Submission>> submissionMap) {
    this.submissionMap = submissionMap;
    fileHandler = new FileHandlerImpl();
  }

  public String getHw_name() {
    return hw_name;
  }

  public void setHw_name(String hw_name) {
    this.hw_name = hw_name;
  }

  @Override
  public void runPlagiarism() throws IOException {
    for(Map.Entry<Languages, List<Submission>> entry : submissionMap.entrySet()) {
      Languages key = entry.getKey();

      if (key.equals(Languages.HTML)){
          parser = factory.createHTMLParser();
      }
      else if(key.equals(Languages.PYTHON)){
          parser = factory.createPythonParser();
      }
      List<Submission> submissionList = entry.getValue();
      // Runs for each type of language.
      for (int i =0; i < submissionList.size(); i++) {
        for (int j = i+1; j < submissionList.size(); j++) {
          Submission sub1 = submissionList.get(i);
          Submission sub2 = submissionList.get(j);
          if (!sub1.getStudent_id().equals(sub2.getStudent_id())) {
            Result plagResult = plagiarismDetectorPython.checkPlagiarism(sub1, sub2, parser);
            results.add(plagResult);
          }
        }
      }
    }
    fileHandler.writeResults(hw_filePath, this.results);
  }

  public List<Result> getResults() {

    return this.results;
  }
}
