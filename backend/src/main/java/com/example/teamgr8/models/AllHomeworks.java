package com.example.teamgr8.models;

import java.io.IOException;
import java.util.List;

/**
 * This handles all the top level homework methods, like creation, updating homeworks with new
 * submissions and deleting homeworks.
 */
public interface AllHomeworks {
    // TODO add method comments.

  boolean createHomeWork(String hwName, String hwFilePath);

  boolean updateHomework(String hwName, String hwFilePath);

  boolean deleteHomework(String hwName);

  List<String> fetchAllHomeworks();

  boolean runPlagiarism(String hwName);
}
