package com.example.teamgr8.models;

import java.util.ArrayList;
import java.util.List;
// TODO add comments and need to implement comparable. 
public class Result implements Comparable<Result> {
    private String student_id1;
    private String student_id2;
    private String filePath1;
    private String filePath2;
    private List<Integer> sub1LineNumbers;
    private List<Integer> sub2LineNumbers;
    private List<String> sub1Lines;
    private List<String> sub2Lines;
    private int matchingLineCount;
    private double sub1Percentage;
    private double sub2Percentage;

    public Result(String student_id1, String student_id2, String filePath1, String filePath2, List<Integer> sub1LineNumbers,
                  List<Integer> sub2LineNumbers, int matchingLineCount, double sub1Percentage, double sub2Percentage,
                  List<String> sub1Lines, List<String> sub2Lines) {
        this.student_id1 = student_id1;
        this.student_id2 = student_id2;
        this.filePath1 = filePath1;
        this.filePath2 = filePath2;
        this.sub1LineNumbers = sub1LineNumbers;
        this.sub2LineNumbers = sub2LineNumbers;
        this.matchingLineCount = matchingLineCount;
        this.sub1Percentage = sub1Percentage;
        this.sub2Percentage = sub2Percentage;
        this.sub1Lines = sub1Lines;
        this.sub2Lines = sub2Lines;
    }

    public Result() {}

    public String getStudent_id1() {
        return student_id1;
    }

    public String getStudent_id2() {
        return student_id2;
    }

    public String getFilePath1() {
        return filePath1;
    }

    public String getFilePath2() {
        return filePath2;
    }

    public List<Integer> getSub1LineNumbers() {
        return sub1LineNumbers;
    }

    public List<Integer> getSub2LineNumbers() {
        return sub2LineNumbers;
    }

    public int getMatchingLineCount() {
        return matchingLineCount;
    }

    public double getSub1Percentage() {
        return sub1Percentage;
    }

    public double getSub2Percentage() {
        return sub2Percentage;
    }

    public List<String> getSub1Lines() {
        return sub1Lines;
    }

    public List<String> getSub2Lines() {
        return sub2Lines;
    }

    public static void main(String[] args) {
        List<Result> results = new ArrayList<>();
        Result result1 = new Result("Student_1","Student2","files/HW1/Student_1/tinycode.py"
        ,"files/HW1/Student_2/tinycode.py",null,null,4,89,
                59,null,null);
        Result result2 = new Result("Student_1","Student2","files/HW1/Student_1/src/tinycode.py"
                ,"files/HW1/Student_2/tinycode.py",null,null,4,89,
                59,null,null);
        Result result3 = new Result("Student_1","Student2","files/HW1/Student_1/src/tinycode.py"
                ,"files/HW1/Student_2/tinycode.py",null,null,4,89,
                59,null,null);
        results.add(result1);
        results.add(result2);
        results.add(result3);
    }

    @Override
    public int compareTo(Result r) {
        double percentage1 = this.getSub1Percentage() + this.getSub2Percentage();
        double percentage2 = r.getSub1Percentage() + r.getSub2Percentage();
        if (percentage1 < percentage2){
            return -1;
        }
        else if(percentage1 > percentage2){
            return 1;
        }
        return 0;
    }
}
