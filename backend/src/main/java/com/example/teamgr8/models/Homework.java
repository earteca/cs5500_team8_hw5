package com.example.teamgr8.models;

import java.io.IOException;

/**
 * Handles all the necessary details for an paticular homework, and runs plagiarism for that
 * homework.
 */
public interface Homework {

  /**
   * Run's plagiarism for every submission of same type agnaist another submission unless its from
   * from same student.
   */
  public void runPlagiarism() throws IOException;
}
