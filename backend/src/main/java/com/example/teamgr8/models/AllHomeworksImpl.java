package com.example.teamgr8.models;

import com.example.teamgr8.utils.FileHandler;
import com.example.teamgr8.utils.FileHandlerImpl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AllHomeworksImpl implements AllHomeworks {

  private Map<String, Homework> allHomeworks;
  private FileHandler fileHandler = new FileHandlerImpl();

  public AllHomeworksImpl() {
    allHomeworks = new HashMap<>();
  }

  @Override
  public boolean createHomeWork(String hwName, String hwFilePath) {
    try {
      Map<Languages, List<Submission>> submissionMap = fileHandler.loadHomework(hwFilePath);
      Homework newHomework = new HomeworkImpl(hwName, hwFilePath, submissionMap);
      allHomeworks.put(hwName, newHomework);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Override
  public boolean updateHomework(String hwName, String hwFilePath) {
    try {
      Map<Languages, List<Submission>> submissionMap = fileHandler.loadHomework(hwFilePath);
      Homework newHomework = new HomeworkImpl(hwName, hwFilePath, submissionMap);
      allHomeworks.put(hwName, newHomework);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Override
  public boolean deleteHomework(String hwName) {
    try {
      allHomeworks.remove(hwName);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Override
  public List<String> fetchAllHomeworks() {
    List<String> keys = (List<String>) allHomeworks.keySet();
    return keys;
  }

  @Override
  public boolean runPlagiarism(String hwName) {
    try {
      Homework homework = allHomeworks.get(hwName);
      homework.runPlagiarism();
      return true;
    } catch (Exception ex) {
      return false;
    }
  }
}
