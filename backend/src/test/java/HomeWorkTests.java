import com.example.teamgr8.models.*;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeWorkTests {

//    @Test
//    public void createHWAndAddSubTest() throws IOException {
//        Map hw1Submissions = new HashMap();
//        Submission sub1 = new Submission("a", "files/tinycode.py");
//        Submission sub2 = new Submission("b", "files/tinycode2.py");
//        Submission sub3 = new Submission("c", "files/tinycode3.py");
//        Submission sub4 = new Submission("d", "files/tinycode4.py");
//        List<Submission> subs = new ArrayList();
//        subs.add(sub1);
//        subs.add(sub2);
//        subs.add(sub3);
//        subs.add(sub4);
//        hw1Submissions.put(Languages.PYTHON, subs);
//        HomeworkImpl hw1 = new HomeworkImpl("hw1", hw1Submissions, );
//        hw1.runPlagiarism();
//        System.out.println(hw1.getResults().size());
//    }

    @Test
    public void allHWTest() {
        AllHomeworks allHW = new AllHomeworksImpl();
        allHW.createHomeWork("hw1", "files/HW1");
        assertTrue(allHW.runPlagiarism("hw1"));
    }
}
