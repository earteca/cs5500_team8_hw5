import com.example.teamgr8.models.Result;
import com.example.teamgr8.models.Submission;
import com.example.teamgr8.utils.*;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class PythonDetectorTests {
    ASTParserFactory factory ;
    ASTParser parser = null;

    public PythonDetectorTests(){
        factory = new ASTParserFactory();
        parser = factory.createPythonParser();
    }

    /**
     * Test two files that are exactly the same.
     *
     * @throws IOException if file not found.
     */
    @Test
    public void exactlyTheSameTest() throws IOException {
        PlagiarismDetector pd = new PlagiarismDetectorImpl();
        Submission sub = new Submission("a", "files/samplefiles/tinycode.py");
        Submission exactlyTheSameSub = new Submission("b", "files/samplefiles/tinycode2.py");
        Result r = pd.checkPlagiarism(sub, exactlyTheSameSub,parser);
        // Matching line count should equal 6
        assertEquals(6, r.getMatchingLineCount());
        // Percentages should be 100 for both
        assertEquals(r.getSub1Percentage(), 100, .01);
        assertEquals(r.getSub2Percentage(), 100, .01);
    }

    /**
     * Test two files that are partially the same.
     *
     * @throws IOException if file not found.
     */
    @Test
    public void partiallyTheSameTest() throws IOException {
        PlagiarismDetector pd = new PlagiarismDetectorImpl();
        Submission sub = new Submission("a", "files/samplefiles/tinycode.py");
        Submission partiallyTheSameSub = new Submission("c", "files/samplefiles/tinycode3.py");
        Result r = pd.checkPlagiarism(sub, partiallyTheSameSub,parser);
        // Matching line count should equal 5
        assertEquals(5, r.getMatchingLineCount());
        // Percentages should be about 83 for both
        assertEquals(83, r.getSub1Percentage(), 1);
        assertEquals(83, r.getSub2Percentage(), 1);
    }

    /**
     * Test two files that are not the same.
     *
     * @throws IOException if file not found.
     */
    @Test
    public void completelyDifferentTest() throws IOException {
        PlagiarismDetector pd = new PlagiarismDetectorImpl();
        Submission sub = new Submission("a", "files/samplefiles/tinycode.py");
        Submission partiallyTheSameSub = new Submission("d", "files/samplefiles/tinycode4.py");
        Result r = pd.checkPlagiarism(sub, partiallyTheSameSub,parser);
        // There should be no matching lines
        assertEquals(0, r.getMatchingLineCount());
        // Percentages should be 0 for both
        assertEquals(0, r.getSub1Percentage(), .01);
        assertEquals(0, r.getSub2Percentage(), .01);
    }

    /**
     * Test a file with code against a file without code.
     *
     * @throws IOException if file not found.
     */
    @Test
    public void noCodeTest() throws IOException {
        PlagiarismDetector pd = new PlagiarismDetectorImpl();
        Submission sub = new Submission("a", "files/samplefiles/tinycode.py");
        Submission emptySub = new Submission("e", "files/samplefiles/tinycodeEmpty.py");
        Result r = pd.checkPlagiarism(sub, emptySub,parser);
        // There should be no matching lines
        assertEquals(0, r.getMatchingLineCount());
        // Percentages should be 0 for both
        assertEquals(0, r.getSub1Percentage(), .01);
        assertEquals(0, r.getSub2Percentage(), .01);
    }

    /**
     * Test two files that are the same but the variable names and string values have been changed.
     *
     * @throws IOException if file not found.
     */
    @Test
    public void sameCodeDifferentNamesTest() throws IOException {
        PlagiarismDetector pd = new PlagiarismDetectorImpl();
        Submission sub = new Submission("d", "files/samplefiles/tinycode4.py");
        Submission emptySub = new Submission("f", "files/samplefiles/tinycode5.py");
        Result r = pd.checkPlagiarism(sub, emptySub,parser);
        // There should be one matching line
        assertEquals(1, r.getMatchingLineCount());
        // Percentages should be 100 for both
        assertEquals(100, r.getSub1Percentage(), .01);
        assertEquals(100, r.getSub2Percentage(), .01);
    }

    /**
     * Test two files where one is twice as large as the other.
     *
     * @throws IOException if file not found.
     */
    @Test
    public void oneFileLargerTest() throws IOException {
        PlagiarismDetector pd = new PlagiarismDetectorImpl();
        Submission sub = new Submission("a", "files/samplefiles/tinycode.py");
        Submission emptySub = new Submission("g", "files/samplefiles/tinycode6.py");
        Result r = pd.checkPlagiarism(sub, emptySub,parser);
        // There should be 6 matching lines
        assertEquals(6, r.getMatchingLineCount());
        // Percentages should be 100 for one and 50 for the other
        assertEquals(100, r.getSub1Percentage(), .01);
        assertEquals(50, r.getSub2Percentage(), .01);
    }

    /**
     * Test two files that are the same but one has comments in it.
     *
     * @throws IOException if file not found.
     */
    @Test
    public void commentsTest() throws IOException {
        PlagiarismDetector pd = new PlagiarismDetectorImpl();
        Submission sub = new Submission("a", "files/samplefiles/tinycode.py");
        Submission emptySub = new Submission("g", "files/samplefiles/tinycodewithcomments.py");
        Result r = pd.checkPlagiarism(sub, emptySub,parser);
        // There should be 6 matching lines
        assertEquals(6, r.getMatchingLineCount());
        // Percentages should be 100 for one and 50 for the other
        assertEquals(100, r.getSub1Percentage(), .01);
        assertEquals(100, r.getSub2Percentage(), .01);
    }

    /**
     * Test two files that have the same code but the second file moved it around and also added other code.
     *
     * @throws IOException if file not found.
     */
    @Test
    public void codeMovedAroundTest() throws IOException {
        PlagiarismDetector pd = new PlagiarismDetectorImpl();
        Submission sub = new Submission("a", "files/samplefiles/tinycode.py");
        Submission emptySub = new Submission("g", "files/samplefiles/tinycode7.py");
        Result r = pd.checkPlagiarism(sub, emptySub,parser);
        // There should be 6 matching lines
        assertEquals(6, r.getMatchingLineCount());
        // Percentages should be 100 for one and 50 for the other
        assertEquals(100, r.getSub1Percentage(), .01);
        assertEquals(55, r.getSub2Percentage(), 1);
    }

    /**
     * Test two files where the code has been extracted out into another method.
     *
     * @throws IOException if file not found.
     */
    @Test
    public void codeExtractedIntoFunctionTest() throws IOException {
        PlagiarismDetector pd = new PlagiarismDetectorImpl();
        Submission sub = new Submission("a", "files/samplefiles/tinycode.py");
        Submission emptySub = new Submission("g", "files/samplefiles/tinycode8.py");
        Result r = pd.checkPlagiarism(sub, emptySub,parser);
        // There should be 6 matching lines
        assertEquals(6, r.getMatchingLineCount());
        // Percentages should be 100 for one and 50 for the other
        assertEquals(100, r.getSub1Percentage(), .01);
        assertEquals(75, r.getSub2Percentage(), 1);
    }
}
